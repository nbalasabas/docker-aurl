{% import 'shared_state/macros.j2' as macro %}

include:
    - shared_state/docker_host

{{
    macro.build_docker(
        [],
        [{"image": "advurl", "context": "se-demos/adv_url_filtering/phobos"}])
}}

adv_url_container:
    docker_container.running:
        - image: {{ grains['master'] }}:5000/advurl:latest 
        - port_bindings:
            - 80:80
