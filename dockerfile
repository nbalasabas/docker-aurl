# Pull base image
FROM ubuntu:18.04
LABEL maintainer="FC <nbalasabas@paloaltonetworks.com>"
LABEL name=docker-aurl

ENV user=poc
ENV group=poc
ENV uid=1000
ENV gid=1000
ENV AGENT_HOME=/home/$user
ENV GIT_TOKEN=cs34uVD2u-3iAfxsNmhL
ENV GIT_USR=poc
ENV GIT_REPO=https://gitlab.com/nbalasabas/docker-aurl
ENV GIT_SSL_NO_VERIFY=1
ENV GIT_LOCALDIR=docker-aurl

RUN groupadd -g $gid $group \
    && useradd -d $AGENT_HOME -u $uid -g $gid -m -s /bin/bash $user

## Install tools
RUN apt-get update \
    && apt-get install -y git \
    && apt-get install -y vim \
    && apt-get install -y iputils-ping \
    && apt-get install -y libvirt-dev \
    && apt-get install -y pkg-config \
    && apt-get install -y sshpass \
    && apt-get install -y mkisofs \
    && apt-get install -y telnet \
    && rm -rf /var/lib/apt/lists/* \
    && git config --global http.sslverify false \
    && git config --global url."https://$GIT_USR:$GIT_TOKEN@gitlab.com/".insteadOf "https://gitlab.com/"

RUN apt-get update \
    #&& apt-get upgrade -y \
    && apt-get install -y curl \
    && apt-get install -y python3.7 \
    && apt-get install -y python3.7-dev \
    && apt-get install -y python3.7-distutils \
    && apt-get install -y python3.7-venv \
    #&& update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 1 \
    #&& update-alternatives --set python3 /usr/bin/python3.7 \
    && curl -kfsSL https://bootstrap.pypa.io/get-pip.py -o get-pip.py \
    && python3.7 get-pip.py --force-reinstall \
    && rm get-pip.py

# Set working directory
WORKDIR $AGENT_HOME

# Install Python packages
COPY requirements.txt .
RUN pip install -U -r requirements.txt

# Clone Git Repo
RUN git clone $GIT_REPO $GIT_LOCALDIR

# Git pull each time container comes up
COPY gitpull.sh .
RUN chmod +x gitpull.sh
